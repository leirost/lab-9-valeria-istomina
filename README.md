# Manual security testing of [Ficbook](https://ficbook.net/)

## Test 1. Forget password ([owasp cheatsheet](https://cheatsheetseries.owasp.org/cheatsheets/Forgot_Password_Cheat_Sheet.html))

| Test step | Expected result | Result|
|---------- | --------------- | ------|
|open [ficbook.net](https://ficbook.net/)|main page opened|Ok, main page opened|
|click "Вход" button|sign in window opened|OK, it is opened|
|click "Забыли пароль?" link|page asks recovery email|OK, page is opened|
|enter my email|page with instructions appears|Ok, it appeared, checking my mail|
|enter a random non-existing email|error message|error message "Пользователя с такой почтой не существует"|
|enter `'; DROP TABLE users; --`|error message|message "email address should contain @ sign"|

## Test 2. XSS ([owasp cheatsheet](https://cheatsheetseries.owasp.org/cheatsheets/Cross_Site_Scripting_Prevention_Cheat_Sheet.html))

| Test step | Expected result | Result|
|---------- | --------------- | ------|
|open [ficbook.net](https://ficbook.net/)|main page opened|Ok, main page opened|
|click "Вход" button|sign in window opened|OK, it is opened|
|enter correct credentials|successful login into account|OK, login successful, main page is opened|
|enter `<script>alert("Vulnerability")</script>` in name search bar|no XSS|message "Совпадений не найдено"|
|enter `<script>alert("XSS Attack!")</script>` in website search bar|no XSS|message "Увы, поиск не дал результатов"|

looks like ficbook is free from cross-site scripting. also i tested this with "add a fanfic" function, but it all is OK, so i decided not to put all testing steps here.

## Test 3. SQL injection ([owasp cheatsheet]())

| Test step | Expected result | Result|
|---------- | --------------- | ------|
|open [ficbook.net](https://ficbook.net/)|main page opened|Ok, main page opened|
|click "Вход" button|sign in window opened|OK, it is opened|
|enter correct credentials|successful login into account|OK, login successful, main page is opened|
|enter `; DROP TABLE users; --` in name search bar|no database updates|message "Совпадений не найдено"|
|enter `; DROP TABLE users; --` in website search bar|no database updates|message "Увы, поиск не дал результатов"|

looks like ficbook is free from sql injection (at least in the testing scope). also i tested this with "add a fanfic" function, but it all is OK, so i decided not to put all testing steps here.

